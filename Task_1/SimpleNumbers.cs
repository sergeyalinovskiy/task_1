﻿namespace Task_1
{
	#region usings
	using System;
	#endregion
	public class SimpleNumbers
	{
		private int firstNumber = 0;
		private int secondNumber = 0;
		private int thirdNumber = 0;
		public void StartSearch()
		{
			while (true)
			{
				Console.WriteLine("Введите число : \n");
				int number;
				string enteredDate = Console.ReadLine();
				if (int.TryParse(enteredDate, out number))
				{
					this.SearchMultiplications(number);
				}
				else
				{
					Console.WriteLine("Вы ввели не чило");
				}
			}
		}

		public bool CheckSimpleNumber(int number)
		{
			bool isSimple = true;
			for (int i = 2; i <= number / 2; i++)
			{
				if (number % i == 0)
				{
					isSimple = false;
					break;
				}
			}
			return isSimple;
		}

		public void SearchMultiplications(int number)
		{
			bool isResult = false;

			int middleNumber = number / 2;
			for (int i = 1; i < middleNumber; i++)
			{
				if (this.CheckSimpleNumber(i))
				{
					if (firstNumber == 0)
					{
						firstNumber = i;
					}
					else if (secondNumber == 0)
					{
						secondNumber = i;
					}
					else if (thirdNumber == 0)
					{
						thirdNumber = i;
					}

					if (firstNumber != 0 && secondNumber != 0 && thirdNumber != 0)
					{
						if (firstNumber * secondNumber * thirdNumber == number)
						{
							isResult = true;
							break;
						}
						else
						{
							isResult = false;
							firstNumber = secondNumber;
							secondNumber = thirdNumber;
							thirdNumber = 0;
						}
					}
				}
			}
			this.PrintResult(isResult);
		}
		
		private void PrintResult(bool isResult)
		{
			if (isResult)
			{
				this.PrintPositiveResult(firstNumber, secondNumber, thirdNumber);
			}
			else
			{
				this.PrintNegativeResult();
			}
		}
		private void PrintPositiveResult(int firstNumber, int secondNumber, int thirdNumber)
		{
			Console.WriteLine("Последовательными простыми множителями данного числа являются: " + firstNumber + "  " + secondNumber + "  " + thirdNumber);
			Console.ReadKey();
		}
		private void PrintNegativeResult()
		{
			Console.WriteLine("Последовательных простых множителей данного числа не обнаружено ");
			Console.ReadKey();
		}
	}
}